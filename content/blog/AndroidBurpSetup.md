---
date: 2020-05-21T10:46:10+03:00
etiketler: "Kurulum"
seriler: "Yazılar"
title: "Android SSL Certificate - Burp Setup"
---

Burp Dashboard ekranında `The Client failed to negotiate an SSL Connection`
hatası veriyorsa Burp CA sertifikasını **SYSTEM** seviyesinde cihaza
yüklenerek sorun aşılabilir.

Nougat ve üzeri sistemlerde geleneksel **USER** seviyesinde sertifika yükleme
işlemiyle uygulamanın yaptığı istekleri yakalamak artık mümkün değil. Android
içerisinde system sertifikaları `/system/etc/security/cacerts` içerisinde
saklanıyor.

```bash
openssl x509 -inform DER -in cacert.der -out cacert.pem
openssl x509 -inform PEM -subject_hash_old -in cacert.pem |head -1
mv cacert.pem <hash>.0
```

```shell
adb root
adb remount
adb push <cert>.0 /sdcard/
```

```shell
mv /sdcard/<cert>.0 /system/etc/security/cacerts/
chmod 644 /system/etc/security/cacerts/<cert>.0
```

TL;DR

```shell
openssl x509 -inform DER -in cacert.der -out cacert.pem
export CERT_HASH=$(openssl x509 -inform PEM -subject_hash_old -in cacert.pem | head -1)
adb root && adb remount
adb push cacert.pem "/sdcard/${CERT_HASH}.0"
adb shell su -c "mv /sdcard/${CERT_HASH}.0 /system/etc/security/cacerts"
adb shell su -c "chmod 644 /system/etc/security/cacerts/${CERT_HASH}.0"
rm cacert.*
```

Muhtemelen telefondaki proxy ayarlarını da ayarlamak istiyorsunuzdur. Onu da
saçma android arayüzü yerine aşağıdaki komut ile değiştirebilirsiniz.

```shell
adb shell settings put global http_proxy 192.168.56.1:8080
```

[Ref](https://blog.ropnop.com/configuring-burp-suite-with-android-nougat/)
