---
date: 2021-02-08T00:00:00+03:00
title: "Systemctl in Docker"
tagler: "Araçlar"
seriler: "Yazılar"
sakla: false
---

`--privileged` argümanı tercih edilmediği durumlarda gereken mountlar aşağıdaki
şekildedir.

Systemd ile docker çalıştırmak için gereken mountlar aşağıdaki gibi
ayarlanabilir. Ayrıca imajdan önce mount edilen noktalarda DISPLAY environment
değişkeni ayarlanarak container içerisinde çalıştırılan GUI içerisindeki
pencereler görülebilir. Aynı zamanda ana dizinde bulunan Share dizini de
konteynır içerisinden erişilebilir.

Sadece GUI uygulamaları görüntüleyebilmek için aşağıdaki şekilde container
çalıştıralabilir.

```bash
docker run -ti -v /tmp/.X11-unix:/tmp/.X11-unix -v $XAUTHORITY:/root/.Xauthority -e DISPLAY archlinux:latest bash
```

Systemd:

```bash
docker run \
      --entrypoint=/usr/lib/systemd/systemd \
      --env container=docker \
      --mount type=bind,source=/sys/fs/cgroup,target=/sys/fs/cgroup \
      --mount type=bind,source=/sys/fs/fuse,target=/sys/fs/fuse \
      --mount type=tmpfs,destination=/tmp \
      --mount type=tmpfs,destination=/run \
      --mount type=tmpfs,destination=/run/lock \
      --net=host --name=Arch --workdir /root -ti \
      --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" -v="$HOME/Share:/root/Share" \
      archlinux:latest --log-level=info --unit=sysinit.target
```

Daha sonrasında terminali kapatıp istenilen komut `docker exec -ti Arch bash`
ile interaktif shell alınabilir. Sonrasında `systemctl status` ile systemd
interaksiyonlarına girilebildiği görülecektir.

Containeri kapatmak için `docker stop Arch` yapmak yerine container içerisinde
`shutdown now` yapmayı unutmamalı :)
