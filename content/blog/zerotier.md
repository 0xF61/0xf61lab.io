---
title: "Zerotier"
date: 2024-09-29T14:37:38Z
tagler: "diğer"
seriler: "Yazılar"
hideSearch: false
---

# Zerotier

[Zerotier](https://www.zerotier.com/), zero trust network kurmak için kullanılabilecek bir
servistir. Farklı networklerde bulunan ve dışarıda merkezi bir sunucusu olmayan
durumlarda cihazlar arası relaying servisi olarak çalışır. Böylece kendi sanal
networkünüze dahil ettiğiniz cihazlara internet erişimi olan veya aynı yerel ağda
olup internete erişebilen bir bridge ile yerel bağlantı kurmanızı sağlar.

Kurulumu gayet basittir, Üye girişi yaptıktan sonra bir network ID'si elde
edilir. Sonrasında cihazlara ilgili platforma ait Zerotier uygulamalarının
kurulumu yapılır. [Link](https://www.zerotier.com/download/).

## Örnek senaryo

Implant edilmiş bir raspberry'e uzaktan bağlanmak için aşağıdaki gibi bir sistem
kullanılabilir.

```fish
curl -s https://install.zerotier.com | sudo bash
```

Kurulum tamamlandıktan sonra Raspberry üzerindeki IP Forwarding kerel parametresi
aşağıdaki komut ile aktifleştirilir.

```fish
sudo sysctl -w net.ipv4.ip_forward=1
```

Site üzerinde 16 karakterli bir network IDsinin agentlara eklenmesi gerekmektedir.

```fish
zerotier-cli join $ZEROTIER_NETWORK_ID
```

Site üzerinden cihaz onaylandıktan sonra bu raspberry'e erişim için kendi IP
adresi kullanarak erişim sağlanabilecektir. Raspberry üzerinden bağlı olduğu
ağdaki cihazlara erişim sağlayabilmek için iptables kuralları ile bu paketlerin
yönlendirilmesi gerekecektir.

`ip a` komutunun çıktılarına göre bridge yapılmak istenen network adları aşağıdaki
gibi tanımlanır.

```fish
PHY_IFACE=eth0; ZT_IFACE=zt7nnig26
```

Aşağıdaki komutlar ile iki network arasında bridge işlemi yapılır.

```fish
sudo iptables -t nat -A POSTROUTING -o $PHY_IFACE -j MASQUERADE
sudo iptables -A FORWARD -i $PHY_IFACE -o $ZT_IFACE -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i $ZT_IFACE -o $PHY_IFACE -j ACCEPT
```

Bundan sonra aşağıdaki komutlar sırası ile çalıştırılarak bağlantı olası bir
elektrik kesintisi sonrasında kalıcı hale getirilir.

```fish
sudo apt install iptables-persistent
sudo bash -c iptables-save > /etc/iptables/rules.v4
```
