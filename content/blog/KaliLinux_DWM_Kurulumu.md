---
title: "Kali Linux DWM kurulumu"
etiketler: "Kurulum"
seriler: "Yazılar"
---

Gerekli paketlerin indirilmesi:

```term
sudo apt install dwm suckless-tools stterm libx11-dev libxft-dev libxinerama-dev fonts-hack-ttf
```

**Opsiyonel:** Benim pathclenmiş dwm ayarlarım:

```term
cd /tmp/
git clone http://gitlab.com/0xF61/dotfiles.git
cd dotfiles/app/dwm
sudo make install
echo "exec /usr/local/bin/dwm" >> ~/.xinitrc
```

_One-Liner_

```
sudo apt install dwm suckless-tools stterm libx11-dev libxft-dev libxinerama-dev fonts-hack-ttf; cd /tmp/; git clone http://gitlab.com/0xF61/dotfiles.git; cd dotfiles/app/dwm; sudo make install; echo "exec /usr/local/bin/dwm" >> ~/.xinitrc;
```
