---
title: "Arch Linux Kurulumu (SecureBoot/systemd-boot/luks/btrfs/wayland)"
date: 2021-12-05T16:38:33+03:00
draft: false
tagler: "Config"
etiketler: "Kurulum"
seriler: "Yazılar"
sakla: false
---

## ISO dosyasının yazdırılması

[Link](https://archlinux.org/download/) adresinden ISO dosyası
indirilir.

`lsblk` ile bağlanılan usb bellek tespit edilerek /dev/sd[a|b|c] ISO
dosyası belleğe yazdırılır.

```bash
# dd bs=4M if=archlinux-version-x86_64.iso of=/dev/sdx conv=fsync oflag=direct status=progress && sync
```

## UEFI Ayarları

Bilgisayar açılırken F13 tuşuna bir kaç kere basarak UEFI/BIOS ayarlarına giriyoruz.

`Security` kategorisi altında öncelikle `Supervisr Password` tanımlıyoruz.
Her sistem başladığında BIOS/UEFI parolası girmeyip kolayca `Evil Maid`lenmek
istiyorsanız bunu yapmayınız.

Yine aynı şekilde `Security` kategorisi altında `Secure Boot` aktifleştirip
bütün kayıtlı secure boot key'lerini sildikten sonra `Setup mode` açılır.

Ayarlar kayıt edilip çıkışş yapılır.

## Boot Me Babe

Sistem yeniden başlarken yine F13 tuşuna basılarak sistemin bellek üzerinden
açılması sağlanır. Umarım UEFI parolanızı hatırlayabilmişsinizdir :).

Grub gelince "Arch Linux install medium ..." (en üstteki) seçilerek devam edilir.

Bir sürü yazı şekil hack filmlerinde kullanılamayacak kadar gerçekçi akan yazılardan
sonra bir adet `root@archiso` prompt'u gelir ve aksiyonumuz başlar.

**Bu aşamada karınca duası okumak istemeyenler font değiştirmek isteyebilir**

```shell
setfont /usr/share/kbd/consolefonts/ter-132n.psf.gz # Tembel olun TAB kullanın
```

## Ethernet girişi/kablosu yok, WiFi'dan çözelim

iwctl ile WiFi bağlantısı yapmak daha kolay :)

```shell
iwctl
device list # Powered on :)
station wlan0 scan
station wlan0 get-networks
station wlan0 connect $SSID
# Bir takım gereksiz parola soruları
exit
```

Bakalım olmuşmu hem dns hem internet erişim kontrolü için ICMP is da way.

```shell
ip a                    # IP adresinin öğrenilmesi SSH atacaklar için önemli bir detay
ping -c 1 archlinux.org # Internet ve DNS paketlerinin çalıştığının kontrol edilmesi
```

## TTY sıktı SSH geç

Internet bağlantısı sağladıktan sonra cihaza SSH bağlantısı sağlayabilmek
için `passwd` ile parola değiştiriyoruz.

Başka bir sistem üzerinden `ssh root@192.168.x.x` ile artık kuruluma devam edilebilir.

## Partitioning

`lsblk` ile daha hiç mount edilmemiş diski tespit ediyorum. Ben nvme bir diske sahip
olduğum için bu disk bende `nvme0n1` olarak gözüküyor. bu noktada kendinize göre disk
adını ayarlamanız gerekmekte.

Diskin üzerinden tertemiz bir geçiş yapıyorum. ( Kısa bir süre de olsa yapmanızı öneririm)

```shell
dd if=/dev/urandom of=/dev/nvme0n1
```

Artık `lsblk` yapıldığında nvme altında partition görmüyorum.

Diski 2 partition'dan daha fazla bölmeyi uygun bulmuyorum. Boot ve ROOT için partitionları
oluşturucam.

```shell
gdisk /dev/nvme0n1
# Bu aşamada MBR buldum düzelteyim mi diye sorarsa Enter ile geçilir
# Command (? for help):        # gdisk için bu prompt anlamına gelmekte
o # GPT tablo oluştur.
y # Evet oluştur son kararım

# /boot
n
1
<Enter>
+200M
EF00

# /
n
<Enter>
<Enter>
<Enter>
<Enter>

w # kayıt ediyoruz
Y # Evet şu aşamada tüm disk tablosunu uçurduğumun farkındayım

# The operation has completed successfully.
```

## LUKS

Şimdi cryptsetup ile diski şifreleyeceğiz fakat bu aşama tamamen size kalmış.
İstenirse direkt olarak `cryptsetup -v -y  luksFormat /dev/nvme0n1p2` devam edilebilir.

**Acı çekmek isteyenler için eklemeler**

`cryptsetup --help` ile en alt satırda varsayılan parametreler göz önünde tutulur.
`cryptsetup benchmark` ile diskten beklenen yazma hızları ve desteklenen algoritmalar çıkartılır.

Beğenilen parametreler aşağıdaki örnekteki yerleri ile değiştirilerek passphrase tanımlanır.

```
cryptsetup --type luks2 --cipher aes-xts-plain64 --hash sha256 --iter-time 2000 --key-size 256 --pbkdf argon2id --use-urandom --verify-passphrase luksFormat /dev/nvme0n1p2
YES      # Köprüden önce son çıkış devamında passphrase girilerek devam edilir.
```

Bu aşama biraz vakit alabilir.

Format aşaması karışmaması için bu disk tekrardan açılarak mount edilir.

```shell
cryptsetup open /dev/nvme0n1p2 luks
```

## Format Partitions

Partitionlar istenen dosya sistemiyle formatlanır. Boot için fat32 ideal.

```shell
mkfs.vfat -F32 /dev/nvme0n1p1
mkfs.btrfs /dev/mapper/luks
```

BTRFS subvolumeleri oluşturularak unmount edilir.

```shell
mount /dev/mapper/luks /mnt
btrfs sub create /mnt/@
btrfs sub create /mnt/@home
umount /mnt
```

BTRFS subvolumelerı kullanılarak tekrardan mount edilir.

```shell
mount -o noatime,nodiratime,compress=zstd:1,space_cache,ssd,subvol=@ /dev/mapper/luks /mnt
# cannot disable free space tree diye ağlarsa
# btrfs check --clear-space-cache v2 /dev/mapper/luks
# sonrasında 1. adımı tekrar et
mkdir -p /mnt/{boot,home}
mount -o noatime,nodiratime,compress=zstd:1,space_cache,ssd,subvol=@home /dev/mapper/luks /mnt/home
mount /dev/nvme0n1p1 /mnt/boot
```

## Pacstrap

Gerekli olan araçları indirmeden önce biraz yeni özellikleri kullanalım.

`vim /etc/pacman.conf` içerisinde `[options]` altındaki satırı `ParallelDownloads = 42`
olarak düzeltiyoruz. Biraz fazla olabilir 17'de fena bir sayı değil :) .

```shell
pacstrap /mnt linux-hardened{,-headers} linux-firmware base base-devel btrfs-progs intel-ucode vim
```

machine-id oluşturalım [;)](https://github.com/Whonix/dist-base-files/blob/master/etc/machine-id)

```shell
echo b08dfa6083e7567a1921a715000001fb > /mnt/etc/machine-id
```

## Chroot Time

Artık chroot atarak sanki boot etmiş gibi sistemimiz düzenleyebiliriz.

```shell
arch-chroot /mnt/
```

Reboot sonrası giriş yapmak isteyebiliriz bu sebeple root parolasını `passwd` ile değiştiriyoruz.

Kullanıcımızı oluşturup temel ayarları yapalım.

```shell
useradd -mG wheel user
passwd user
visudo # %wheel ALL=(ALL) ALL
```

Standart sistem ayarlarını yapalım

```shell
echo host > /etc/hostname
echo en_US.UTF-8 UTF-8  >> /etc/locale.gen
locale-gen
ln -sf /usr/share/zoneinfo/UTC /etc/localtime # 3 saat ekleyin temel matematik
hwclock --systohc
```

/etc/hosts

```shell
127.0.0.1 localhost
::1 localhost
127.0.1.1 <YOUR-HOSTNAME>.localdomain <YOUR-HOSTNAME>
```

/etc/pacman.conf

```shell
Color
CheckSpace
VerbosePkgLists
ILoveCandy
ParallelDownloads = 17
```

vim /etc/mkinitcpio.conf # Sadece değişecek satırları yazıyorum

```shell
MODULES=(btrfs)
HOOKS=(base udev systemd autodetect keyboard modconf block sd-encrypt filesystems)
```

`mkinitcpio -P` ile tüm kernel initramfs'lerimizi oluşturalım.

Temel userspace paketlerini de kuralım.

```shell
pacman -S alsa-utils apparmor dialog dosfstools fish git mtools networkmanager pipewire pipewire-alsa pipewire-pulse sbctl hyprland wpa_supplicant xdg-user-dirs xdg-utils
```

Bir süre pacman videosu izlenir ve indirilen servisler aktifleştirilir.

```shell
systemctl enable NetworkManager apparmor
```

## Bootloader (systemd-boot)

Kurulumu basit ve hızlı çalışan systemd-boot kullanıyorum. (no hate grub)

`bootctl --path=/boot install` ile kurulumu gerçekleştiriyorum.

Konfigürasyonunda UUID kullanacağım için elle yazmamak için önceden dosya içerisine atıyorum.

```shell
blkid -s UUID -o value /dev/nvme0n1p2 > /boot/loader/entries/arch.conf
```

vim /boot/loader/entries/arch.conf

```shell
title Arch Linux
linux /vmlinuz-linux-hardened
initrd /intel-ucode.img
initrd /initramfs-linux-hardened.img
options rd.luks.name=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX=luks root=/dev/mapper/luks rootflags=subvol=@ rd.luks.options=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX=discard rw quiet lsm=lockdown,yama,apparmor,bpf
```

vim /boot/loader/loader.conf

```shell
default arch.conf
editor no
```

## Secureboot

Secure boot için gereken araçları daha önceden indirdik.

`sbctl status` ile duruma bakabiliriz. 3 tane çarpı ile yolumuzu ayırmış olmamız gerekiyor.

```shell
Installed:      ✗ Sbctl is not installed
Setup Mode:     ✗ Enabled
Secure Boot:    ✗ Disabled
```

Öncelikle secureboot anahtarlarımızı oluşturup yazalım.

```shell
sbctl create-keys
sbctl enroll-keys
sbctl status
```

Oluşturduğumuz kernel imajlarını imzalayalım.

```shell
sbctl sign /boot/EFI/BOOT/BOOTX64.EFI
sbctl sign /boot/EFI/systemd/systemd-bootx64.efi
sbctl sign /boot/vmlinuz-linux-hardened
```

Tüm imajlar imzalanmış olmalı.

```shell
sbctl verify
```

## Final Touch

Tek atımlık kurşunumuzu test etme zamanı. Chroottan çıkıp diskleri unmount edip yeniden başlatıyoruz.

```shell
exit
umount -a
reboot
```
