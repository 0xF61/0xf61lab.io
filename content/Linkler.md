---
title: "Linkler"
gizle: "e"
HideSearch: true
---

[Ali Gören](https://aligoren.com/): Açık Kaynak, JavaScript, Python, .NET

[Enes Ergün](https://enesergun.net/): Junior of All

[Mertcan GÖKGÖZ](https://mertcangokgoz.com/)
( [wiki](http://wiki.mertcangokgoz.com/) ): Linux ve Windows sistem uzmanlığı

[Onur Aslan](https://onur.im/): He uses The Universal Operating System
